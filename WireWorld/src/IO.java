import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;



public class IO {
	
	
	public static void readFile (File file, Engine engine) throws IOException {
		
			String state1 = new String("Conductor:");
			String state2 = new String("ElectronHead:");
			String state3 = new String("ElectronTail:");
			
			Scanner sc = new Scanner(file);
			while (true) {
				if (sc.hasNext(state1)) {
					sc.next(state1);
					engine.spawnCell(sc.nextInt(), sc.nextInt(), new Conductor());
				}
				else if (sc.hasNext(state2)) {
					sc.next(state2);
					engine.spawnCell(sc.nextInt(), sc.nextInt(), new ElectronHead());
				}
				else if (sc.hasNext(state3)) {
					sc.next(state3);
					engine.spawnCell(sc.nextInt(), sc.nextInt(), new ElectronTail());
				}
				else break;
			}
			sc.close();
	}
	
	public static void writeFile(File file, Engine engine) throws IOException
    {
		PrintWriter writer = new PrintWriter(file);
		ArrayList<Cell> tmp = engine.getCells();
		for (Cell c: tmp) {
			writer.println(c.getState().getClass().getName() + ": " + c.getX() + " " + c.getY());
		}
		writer.close();
    }


}
