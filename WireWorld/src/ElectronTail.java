import java.awt.Color;


public class ElectronTail implements State, Element {

	@Override
	public State setState(int electronHeadEnvirons) {
		return new Conductor();
		
	}

	@Override
	public Color getColor() {
		return Color.RED;
	}

	@Override
	public void pullElement(int x, int y, Engine engine) {
		engine.spawnCell(x, y, new ElectronTail());
		
	}

}
