
public class ElementFactory {
	  public static Element spawnElement(String s)
	    {
	        switch (s)
	        {
	            case "ElectronHead":
	                return new ElectronHead();
	            case "Electron Head":
	                return new ElectronHead();
	            case "ElectronTail":
	                return new ElectronTail();
	            case "Electron Tail":
	                return new ElectronTail();
	            case "Insulator":
	            	return new Insulator();
	            case "Conductor":
	                return new Conductor();
	            case "Diode":
	                return new Diode();
	            case "OR":
	                return new OR();
	            case "NAND":
	                return new NAND();
	            default:
	                System.err.println("Zignorowana linia \"" + s + "\" - nie istnieje taki element");
	        }
	        return null;
	    }
}
