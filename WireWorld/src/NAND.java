
public class NAND implements Element {
		 
	    @Override
	    public void pullElement(int x, int y, Engine engine)
	    {
	        Element conductor = new Conductor();
	        conductor.pullElement(x, y, engine);
	        conductor.pullElement(x+1, y-1, engine);
	        conductor.pullElement(x+2, y-1, engine);
	        conductor.pullElement(x+3, y, engine);
	        conductor.pullElement(x+3, y+1, engine);
	        conductor.pullElement(x+2, y+1, engine);
	        conductor.pullElement(x+3, y+2, engine);
	        conductor.pullElement(x+2, y+3, engine);
	        conductor.pullElement(x+1, y+3, engine);
	        conductor.pullElement(x, y+2, engine);
	        conductor.pullElement(x+4, y+3, engine);
	        conductor.pullElement(x+5, y+3, engine);
	        conductor.pullElement(x+4, y+1, engine); }
	}

