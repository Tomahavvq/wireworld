import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Insets;
import java.io.File;
import java.io.IOException;

import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;

import java.awt.Rectangle;
import java.awt.Dimension;

import javax.swing.JCheckBox;
import javax.swing.JToolBar;
import javax.swing.JSlider;

import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;


public class GUI {
	
	final static int WIDTH = 1024;
	final static int HEIGHT = 768;
	
	private static GridDisplay gridDisplay;
	private Element element;
	
	public static Engine engine = new Engine();
	
	final JFileChooser fc = new JFileChooser();
	
    private JFrame frmWireWorld;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmWireWorld.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		//initialize();
//	}

	/**
	 * Initialize the contents of the frame.
	 */
	//private void initialize() {
		frmWireWorld = new JFrame();
		frmWireWorld.setTitle("Wire World");
		frmWireWorld.setBounds(100, 100, WIDTH, HEIGHT);
		frmWireWorld.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmWireWorld.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		mnFile.setIcon(new ImageIcon(GUI.class.getResource("/com/sun/java/swing/plaf/gtk/icons/Directory.gif")));
		menuBar.add(mnFile);
		
		final JMenuItem mntmOpen = new JMenuItem("Open");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == mntmOpen) open();
			}
		});
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		mnFile.add(mntmSave);
		
		JButton StartButton = new JButton("");
		StartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				engine.start();
			}
		});
		StartButton.setMargin(new Insets(0, 0, 0, 0));
		StartButton.setContentAreaFilled(false);
		StartButton.setBorderPainted(false);
		StartButton.setToolTipText("Start");
		StartButton.setForeground(UIManager.getColor("text"));
		StartButton.setBackground(Color.WHITE);
		StartButton.setIcon(new ImageIcon("img/play.png"));
		menuBar.add(StartButton);
		
		JButton PauseButton = new JButton("");
		PauseButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				engine.pause();
			}
		});
		PauseButton.setToolTipText("Pause");
		PauseButton.setIcon(new ImageIcon("img/pause.png"));
		PauseButton.setContentAreaFilled(false);
		PauseButton.setBorderPainted(false);
		PauseButton.setMargin(new Insets(0, 0, 0, 0));
		menuBar.add(PauseButton);
		
		JButton StopButton = new JButton("");
		StopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				engine.stop();
			}
		});
		StopButton.setIcon(new ImageIcon("img/stop.png"));
		StopButton.setContentAreaFilled(false);
		StopButton.setBorderPainted(false);
		StopButton.setMargin(new Insets(0, 0, 0, 0));
		StopButton.setVerifyInputWhenFocusTarget(false);
		StopButton.setToolTipText("Stop");
		menuBar.add(StopButton);
		
		JCheckBox chckbxHideNet = new JCheckBox("Hide Net");
		chckbxHideNet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gridDisplay.toggleNet();
			}
		});
		menuBar.add(chckbxHideNet);
		
		JPanel panel = new JPanel();
		frmWireWorld.getContentPane().add(panel, BorderLayout.WEST);
		gridDisplay = new GridDisplay(824, 768, engine);
		gridDisplay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				gridClicked(e);
			}
		});
		panel.add(gridDisplay);
		
		JPanel panel_1 = new JPanel();
		panel_1.setPreferredSize(new Dimension(180, 768));
		frmWireWorld.getContentPane().add(panel_1, BorderLayout.EAST);
		
		final JComboBox elementChooser = new JComboBox();
		elementChooser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tmp = elementChooser.getSelectedItem().toString();
				element = ElementFactory.spawnElement(tmp);
				
			}
		});
		elementChooser.setPreferredSize(new Dimension(170, 30));
		elementChooser.addItem("Conductor");
		elementChooser.addItem("Electron Head");
		elementChooser.addItem("Electron Tail");
		elementChooser.addItem("Insulator");
		elementChooser.addItem("Diode");
		elementChooser.addItem("OR");
		elementChooser.addItem("NAND");
		panel_1.add(elementChooser);
		
		JLabel lblNewLabel = new JLabel("Interval [Ms]");
		panel_1.add(lblNewLabel);
		
		final JFormattedTextField formattedTextField = new JFormattedTextField();
		formattedTextField.setValue(new Integer(300));
		formattedTextField.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				engine.setStepInterval((((Number)formattedTextField.getValue()).intValue()));
			}
		});
		panel_1.add(formattedTextField);
		
		JLabel lblSquareSize = new JLabel("Square Size");
		panel_1.add(lblSquareSize);
		
		final JFormattedTextField formattedTextField_1 = new JFormattedTextField();
		formattedTextField_1.setValue(new Integer(gridDisplay.getSquareSize()));
		formattedTextField_1.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				gridDisplay.setSquareSize(((Number)formattedTextField_1.getValue()).intValue());
			}
		});
		panel_1.add(formattedTextField_1);

		
		
	}
	void gridClicked(MouseEvent e) {
        int x = gridDisplay.coordToPos(e.getX());
        int y = gridDisplay.coordToPos(e.getY());
        gridDisplay.changeCellState(x, y, element);
    }
	
	
    private void open() {
    	int returnVal = fc.showOpenDialog(frmWireWorld);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
				IO.readFile(file, engine);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    private void save() {
    	int returnVal = fc.showOpenDialog(frmWireWorld);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
				IO.writeFile(file, engine);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    
    
}
