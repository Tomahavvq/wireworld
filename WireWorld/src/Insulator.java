
public class Insulator implements Element {

	@Override
	public void pullElement(int x, int y, Engine engine) {
		if (engine.getCellAt(x, y) != null) {
			engine.removeCell(engine.getCellAt(x, y));
		}
	}

}
