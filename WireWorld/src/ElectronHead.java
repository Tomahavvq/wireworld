import java.awt.Color;


public class ElectronHead implements State, Element {

	@Override
	public State setState(int electronHeadEnvirons) {
		return new ElectronTail();
		
	}

	@Override
	public Color getColor() {
		return Color.BLUE;
	}

	@Override
	public void pullElement(int x, int y, Engine engine) {
		engine.spawnCell(x, y, new ElectronHead());
	}

}
