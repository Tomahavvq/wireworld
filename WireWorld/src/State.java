import java.awt.Color;


public interface State {
	State setState (int electronHeadEnvirons);
	Color getColor();
	
}
