
public class Diode implements Element {

	 @Override
	    public void pullElement(int x, int y, Engine engine)
	    {
	        Element conductor = new Conductor();
	        conductor.pullElement(x, y, engine);
	        conductor.pullElement(x - 1, y, engine);
	        conductor.pullElement(x - 2, y, engine);
	        conductor.pullElement(x, y - 1, engine);
	        conductor.pullElement(x, y + 1, engine);
	        conductor.pullElement(x + 1, y - 1, engine);
	        conductor.pullElement(x + 2, y, engine);
	        conductor.pullElement(x + 1, y + 1, engine);
	        conductor.pullElement(x + 3, y, engine);
	    }
}
