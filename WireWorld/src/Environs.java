
public class Environs {
	
	public static void setEnvirons(Cell c, Engine engine) {
		
		int[][] envCoords =  { {c.getX(), c.getY()-1}, {c.getX()+1, c.getY()-1}, {c.getX()+1, c.getY()}, {c.getX()+1, c.getY()+1},
                {c.getX(), c.getY()+1}, {c.getX()-1, c.getY()+1}, {c.getX()-1, c.getY()}, {c.getX()-1, c.getY()-1} };
		c.setElectronHeadEnviron(0);
		int electronHeadEnvirons = 0;
		for (int[] coords : envCoords) {
			Cell tmp = engine.getCellAt(coords[0], coords[1]);
			if (tmp == null) continue;
			else {
				if (tmp.getState().getClass().getName() == "ElectronHead") electronHeadEnvirons++;
			}
			
		}
		c.setElectronHeadEnviron(electronHeadEnvirons);
	}
}
