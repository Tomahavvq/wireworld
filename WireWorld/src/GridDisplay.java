import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;


public class GridDisplay extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width;
	private int height;
	
	private Engine engine;
	
	public GridDisplay (int width, int height, Engine engine) {
		
		this.width = width;
		this.height = height;
		this.engine = engine;
		
		setSquareSize(squareSize);
		setPreferredSize(new Dimension(width, height));
		setBackground(Color.BLACK);
		
		  Timer timer = new Timer(repaintInterval, new TimerListener());
	      timer.start();
	}
	
	private int drawNet = 1;
	private int squareSize = 15;
	private int repaintInterval = 15;
	
	private int center, size;
	
	public void paintComponent (Graphics g) {
		
		super.paintComponent(g);
		
		if (drawNet == 1) {
			g.setColor(new Color(100,100,100));
			int circuit, i;
			for (i=1; i<=size; i++) {
				circuit = i * squareSize;
				g.drawLine(circuit, 0, circuit, height);
                g.drawLine(0, circuit, width, circuit);
			}
			
		}
		
		int x,y;
		for (Cell cell: engine.getCells()) {
			 g.setColor(cell.getState().getColor());
			 x = center + cell.getX();
	         y = center + cell.getY();
	         g.fillRect(x*squareSize, y*squareSize, squareSize, squareSize);
		}
	}
	 
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
	            repaint();
	        }
	    }

	public void setSquareSize(int squareSize) {
		if (squareSize < 1) squareSize = 1;
        this.squareSize = squareSize;
        size = width / squareSize;
        center = size/2;
	}

	public int coordToPos(int coord) {
		int centerCoord = (center) * squareSize;
		if (coord > centerCoord) {
			return (coord-centerCoord)/squareSize;
			} else {
				return ((centerCoord-coord)/squareSize * -1) -1;
	        }
	    }
	
	public void changeCellState(int x, int y, Element element) {
		Cell c = engine.getCellAt(x, y);
		element.pullElement(x, y, engine);
		
	
	}
	public void toggleNet() {
        drawNet = (drawNet==1) ? 0 : 1;
    }
	
	public int getSquareSize() {
		return squareSize;
	}

	
}
