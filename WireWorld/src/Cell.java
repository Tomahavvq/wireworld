
public class Cell {
	
	private int x;
	private int y;
	
	private int electronHeadEnvirons;
	
	private State state;

	public Cell (int x, int y, State state) {
		this.x = x;
		this.y = y;
		this.state = state;
	}
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public int getElectronHeadEnviron() {
		return electronHeadEnvirons;
	}

	public void setElectronHeadEnviron(int electronHeadEnviron) {
		this.electronHeadEnvirons = electronHeadEnviron;
	}
	
	
}
