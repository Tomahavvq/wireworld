import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Timer;


public class Engine {

	private ArrayList <Cell> cells;
	public HashMap <Coords, Cell> cellMap;
	
	public static final int ON = 1;
	public static final int OFF = 0;
	
	private int state = OFF; 
	private int stepInterval = 300;
	private int stepCount = 0;
	
	private Timer timer;
	
	public Engine () {
		cellMap = new HashMap <Coords, Cell> ();
		cells = new ArrayList <Cell>();
	}
	
	public void spawnCell(int x, int y, State state) {
		Cell c = new Cell(x, y, state);
		cellMap.put(new Coords(x, y), c);
		cells.add(c);
	}
	
	public Cell getCellAt(int x, int y) {
		return cellMap.get(new Coords(x,y));
	}
	
	public ArrayList<Cell> getCells() {
		return cells;
	}
	
	private void step() {
		stepCount++;
		for (Cell c: cells) {
			Environs.setEnvirons(c, this);
		}
		for (Cell c: cells) {
			c.setState(c.getState().setState(c.getElectronHeadEnviron()));
		}
	}
	
	public int getStepCount() {
		return stepCount;
	}
	
	public int getStepInterval() {
		return stepInterval;
	}
	
	public void setStepInterval(int stepInterval) {
		this.stepInterval = Math.max(10, stepInterval);
		pause();
		start();
	}
	
	class TimerListener implements ActionListener {
		public void actionPerformed (ActionEvent e) {
			step();
		}
	}
	
	public void removeCell (Cell c) {
		cells.remove(c);
		cellMap.remove(new Coords(c.getX(), c.getY()));
	}
	public boolean start() {
		if (state == OFF) {
			timer = new Timer(stepInterval, new TimerListener());
			timer.start();
			state = ON;
			return true;
		}
		else return false;
	}
	public boolean pause() {
		if (state == ON) {
			timer.stop();
			state = OFF;
			return true;
		}
		else return false;
	}
	
	public void stop() {
		cellMap.clear();
		cells.clear();
		stepCount = 0;
	}
	
	final class Coords {
		
		private final int x;
		private final int y;
		
		public Coords(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public boolean equals(Object other) {
			Coords that = (Coords) other;
			return (this.x == that.x && this.y == that.y);
		}
		
		public int hashCode() {
			return x * 533772 + y;
		}
	}
}
