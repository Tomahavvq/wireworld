import java.awt.Color;


public class Conductor implements State, Element {

	@Override
	public State setState(int electronHeadEnvirons) {
		if (electronHeadEnvirons == 1 || electronHeadEnvirons == 2) {
			return new ElectronHead();
		}
		return new Conductor();
		
	}

	@Override
	public Color getColor() {
		return Color.YELLOW;
	}

	@Override
	public void pullElement(int x, int y, Engine engine) {
		engine.spawnCell(x, y, new Conductor());
	}

}
